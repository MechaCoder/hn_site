(function($){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        items:1,
        merge:true,
        autoplay: true,
        autoplayTimeout: 1000,
        autoplayHoverPause:true
    })

})(jQuery);