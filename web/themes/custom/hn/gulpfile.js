'use strict'

var gulp = require('gulp');
var sass = require('gulp-sass');
var cont = require('gulp-concat');

gulp.task('default', ['sass', 'js']);

gulp.task('js', ['js:theme', 'js:app'])

gulp.task('sass', function(){

    return gulp.src('./a/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./a/css'));
})

gulp.task('js:app', function(){

    var listOfFiles = [
        // './node_modules/jquery/dist/jquery.min.js',
        './node_modules/owl.carousel/dist/owl.carousel.min.js'
    ]
    return gulp.src(listOfFiles)
        .pipe(cont('app.js'))
        .pipe(gulp.dest('./a/js/'));
});

gulp.task('js:theme', function(){
    var listOfFiles = [
        './a/js/src/*.js'
    ]

    return gulp.src(listOfFiles)
        .pipe(cont('theme.js'))
        .pipe(gulp.dest('./a/js/'));
});